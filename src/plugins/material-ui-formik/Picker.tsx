import { DatePicker } from '@material-ui/pickers'
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date'
import { FieldProps } from 'formik'

export type PickerProps = FieldProps & {
  format?: string
  allowEmpty?: boolean
}

export default function Picker(props: PickerProps) {
  const {
    field,
    form,
    format = 'dd.MM.yyyy',
    allowEmpty = false,
    ...otherProps
  } = props

  // converts value to timestamp of seconds
  const initialValue = allowEmpty ? null : Date.now()
  const value = field.value ? field.value * 1000 : initialValue

  // use on change callback
  const onChange = (date: MaterialUiPickersDate) => {
    form.setFieldValue(field.name, Date.parse(String(date)) / 1000)
  }

  return (
    <DatePicker
      {...otherProps}
      name={field.name}
      value={value}
      format={format}
      fullWidth
      inputVariant="outlined"
      onChange={onChange}
      clearable={allowEmpty}
    />
  )
}
