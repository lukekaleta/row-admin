export type OrdersModel = {
  name: string
  surname: string
  phone: string
  email: string
  companyName: string
  companyIco: string
  city: string
  createdAt: number
  message: string
  hasDomain: boolean
  hasHosting: boolean
  webVariant: string
}
