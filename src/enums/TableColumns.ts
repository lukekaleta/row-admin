export enum TableColumnsAligns {
  Left = 'left',
  Center = 'center',
  Right = 'right',
  Inherit = 'inherit',
  Justify = 'justify',
}

export enum TableColumnsPaddings {
  Checkbox = 'checkbox',
  Default = 'default',
  None = 'none',
}

export enum TableColumnsSizes {
  Medium = 'medium',
  Small = 'small',
}
