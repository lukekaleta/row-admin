export type IconProps = {
  className?: string
  size?: string
}