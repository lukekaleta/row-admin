export interface MenuData {
  id?: string
  menuName: string
  menuPrice: string
  menuAmount: string
  menuAllergens: string[] | []
  hasMinute: boolean
  hasBusiness: boolean
}

export interface DailyMenuDay {
  menuDate: number
  lastUpdate: number
  hasDayCanceled?: boolean
  reasonForCancel?: string
  soapName: string
  soapPrice: string
  soapAllergens: string
  soapAmount: number
}

export interface DailyMenuData {
  monday: DailyMenuDay
  tuesday: DailyMenuDay
  wednesday: DailyMenuDay
  thursday: DailyMenuDay
  friday: DailyMenuDay
  saturday: DailyMenuDay
  sunday: DailyMenuDay
}

// NEW ONE
export interface IDailyMenuSoup {
  id?: string
  name: string
  amount: number
  price: number
  allergens: string[] | []
}

export interface IDailyMenu {
  id?: string
  number?: number
  name: string
  allergens: string[] | []
  amount: number
  price: number
  isBusiness: boolean
}

export interface IDailyMenuInfo {
  id?: string
  menuDate: string
  description: string
  isCanceled: boolean
  reasonForCancel: string
  soups: IDailyMenuSoup[] | []
  menu: IDailyMenu[] | []
}
