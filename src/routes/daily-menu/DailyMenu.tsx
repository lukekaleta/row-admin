import { Tab } from '@material-ui/core'
import { TabContext, TabList, TabPanel } from '@material-ui/lab'
import { makeStyles } from '@material-ui/styles'
import { ChangeEvent, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Collections, Documents } from '../../remote/Collections'
import DailyMenuItems from './DailyMenuItems'
import { dailyMenuStyles } from './DailyMenu@styles'

const useStyles = makeStyles(dailyMenuStyles)

const DailyMenu = () => {
  // state
  const [value, setValue] = useState<string>('monday')

  // hooks
  const classes = useStyles()
  const { t } = useTranslation()

  // settings
  const collection = Collections.Daily_Menu
  const documentsMonday = Documents.Monday
  const documentsTuesday = Documents.Tuesday
  const documentsWednesday = Documents.Wednesday
  const documentsThursday = Documents.Thursday
  const documentsFriday = Documents.Friday
  const documentsSaturday = Documents.Saturday
  const documentsSunday = Documents.Sunday

  const handleChangeTab = (event: ChangeEvent<{}>, newValue: string) => {
    setValue(newValue)
  }

  const tabsList = [
    {
      label: t`Common.Days.Monday`,
      value: 'monday',
    },
    {
      label: t`Common.Days.Tuesday`,
      value: 'tuesday',
    },
    {
      label: t`Common.Days.Wednesday`,
      value: 'wednesday',
    },
    {
      label: t`Common.Days.Thursday`,
      value: 'thursday',
    },
    {
      label: t`Common.Days.Friday`,
      value: 'friday',
    },
    {
      label: t`Common.Days.Saturday`,
      value: 'saturday',
    },
    {
      label: t`Common.Days.Sunday`,
      value: 'sunday',
    },
  ]

  const tabPanels = [
    {
      value: 'monday',
      content: (
        <DailyMenuItems collection={collection} documents={documentsMonday} />
      ),
    },
    {
      value: 'tuesday',
      content: (
        <DailyMenuItems collection={collection} documents={documentsTuesday} />
      ),
    },
    {
      value: 'wednesday',
      content: (
        <DailyMenuItems
          collection={collection}
          documents={documentsWednesday}
        />
      ),
    },
    {
      value: 'thursday',
      content: (
        <DailyMenuItems collection={collection} documents={documentsThursday} />
      ),
    },
    {
      value: 'friday',
      content: (
        <DailyMenuItems collection={collection} documents={documentsFriday} />
      ),
    },
    {
      value: 'saturday',
      content: (
        <DailyMenuItems collection={collection} documents={documentsSaturday} />
      ),
    },
    {
      value: 'sunday',
      content: (
        <DailyMenuItems collection={collection} documents={documentsSunday} />
      ),
    },
  ]

  return (
    <div className={classes.root}>
      <TabContext value={value}>
        <TabList
          onChange={handleChangeTab}
          textColor="primary"
          indicatorColor="primary"
          aria-label="simple tabs example"
        >
          {tabsList.map((tab, index) => (
            <Tab key={index} label={tab.label} value={tab.value} />
          ))}
        </TabList>

        {tabPanels.map((tabPanel, index) => (
          <TabPanel
            color="primary"
            key={index}
            value={tabPanel.value}
            className={classes.tabPanel}
          >
            {tabPanel.content}
          </TabPanel>
        ))}
      </TabContext>
    </div>
  )
}

export default DailyMenu
