import { Collections, Documents } from '../../remote/Collections'
import { DailyMenuDay, IDailyMenu } from '../../types/daily-menu.types'

export type DailyMenuItemsStateTypes = {
  dailyMenuDay: DailyMenuDay | null
}

export type DailyMenuItemPropsTypes = {
  collection: Collections
  documents: Documents
}

export type MenuSettingsPropsTypes = {
  handleRemove: (id: string) => void
  handleUpdate: (id: string, data: IDailyMenu, index: number) => void
  data: any
  index: number
}
