import { makeStyles } from '@material-ui/styles'
import {
  Paper,
  Typography,
  Grid,
  Button,
  TextField,
  InputAdornment,
} from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { partsStyles } from './DailyMenu@styles'
import allergens from '../../lib/local-storage/static-data/allergens'
import Tag from '../../components/Tag'
import { IDailyMenuSoup } from '../../types/daily-menu.types'
import React, { useState } from 'react'

interface ISoupProps {
  handleRemove: (id: string) => void
  handleUpdate: (id: string, data: IDailyMenuSoup) => void
  data: IDailyMenuSoup
  index: number
}

const useStyles = makeStyles(partsStyles)

const Soup = (props: ISoupProps) => {
  // props
  const { handleRemove, handleUpdate, data, index } = props

  // state
  const [state, setState] = useState<IDailyMenuSoup>({
    name: data.name,
    amount: data.amount,
    price: data.price,
    allergens: data.allergens,
  })

  // hooks
  const { t } = useTranslation()
  const classes = useStyles()

  const soupId = data?.id || ''

  /**
   * If is allergen in state, system detect active allergen
   * @param identifier
   * @returns
   */
  const checkActiveAllergen = (identifier: string): boolean => {
    const arr: string[] = [...state.allergens]
    return arr.includes(identifier) || false
  }

  /**
   * If is active allergen and user click on allergen again,
   * system will remove that allergen from state so result is
   * current active allergens. Also if user click on unactive allergen
   * system will add that allergen to state and results is active allergens
   * @param identifier
   * @returns
   */
  const handleAddAllergen = (identifier: string): void => {
    if (checkActiveAllergen(identifier)) {
      return setState((prevState) => ({
        ...prevState,
        allergens: state.allergens.filter((item) => {
          return item !== identifier
        }),
      }))
    } else {
      return setState((prevState) => ({
        ...prevState,
        allergens: [...state.allergens, identifier],
      }))
    }
  }

  /**
   * After click on button on existing soup, data will be updated
   * @returns
   */
  const handleUpdateSoup = (): void => {
    if (data.id) {
      return handleUpdate(data.id, state)
    }
  }

  /**
   * Field text changer
   * @param event
   * @returns
   */
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    return setState((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }))
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Typography variant="overline" gutterBottom>
            {`${t('Daily_Menu.Soap')} ${index + 1}`}
          </Typography>
          <div className={classes.spacing}>
            <Grid container spacing={1}>
              <Grid item md={2} xs={12}>
                <TextField
                  id="amount"
                  name="amount"
                  label={t`Daily_Menu.Amount`}
                  variant="outlined"
                  value={state.amount}
                  onChange={handleChange}
                  fullWidth
                  type="number"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">l</InputAdornment>
                    ),
                  }}
                />
              </Grid>

              <Grid item md={8} xs={12}>
                <TextField
                  id="name"
                  name="name"
                  label={t`Daily_Menu.Soap_Name`}
                  variant="outlined"
                  value={state.name}
                  onChange={handleChange}
                  fullWidth
                />
              </Grid>

              <Grid item md={2} xs={12}>
                <TextField
                  id="price"
                  name="price"
                  label={t`Daily_Menu.Price`}
                  variant="outlined"
                  value={state.price}
                  onChange={handleChange}
                  type="number"
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">Kč</InputAdornment>
                    ),
                  }}
                />
              </Grid>

              <Grid item md={8} xs={12}>
                <div className={classes.left}>
                  <Typography variant="subtitle1">
                    {t('Daily_Menu.Allergens') + ':'}
                  </Typography>
                  {allergens.map((a) => (
                    <Tag
                      key={a.number}
                      identifier={a.identifier}
                      label={String(a.number)}
                      tooltip={t(`Common.Allergens.${a.loc}`)}
                      onClick={handleAddAllergen}
                      isActive={checkActiveAllergen(a.identifier)}
                    />
                  ))}
                </div>
              </Grid>

              <Grid item md={2} xs={12}>
                <Button
                  variant="text"
                  color="secondary"
                  size="medium"
                  onClick={() => handleRemove(soupId)}
                  fullWidth
                >
                  {t('Global.Remove')}
                </Button>
              </Grid>
              <Grid item md={2} xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  size="medium"
                  onClick={handleUpdateSoup}
                  fullWidth
                >
                  {t('Global.Save')}
                </Button>
              </Grid>
            </Grid>
          </div>
        </Paper>
      </Grid>
    </Grid>
  )
}

export default Soup
