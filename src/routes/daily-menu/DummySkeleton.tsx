import { Grid, Paper, Typography, TextField } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { makeStyles } from '@material-ui/styles'
import { partsStyles } from './DailyMenu@styles'

const useStyles = makeStyles(partsStyles)

const DummySkeleton = () => {
  // hooks
  const classes = useStyles()

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item md={3} xs={12}>
          <Paper className={classes.paper}>
            <Skeleton>
              <Typography variant="overline" gutterBottom>
                Dummy header
              </Typography>
            </Skeleton>
            <Skeleton>
              <TextField
                id="dummy-input"
                name="menuAmount"
                variant="outlined"
                fullWidth
              />
            </Skeleton>
          </Paper>
        </Grid>
        <Grid item md={9} xs={12}>
          <Paper className={classes.paper}>
            <Skeleton>
              <Typography variant="overline" gutterBottom>
                Dummy header
              </Typography>
            </Skeleton>
            <Skeleton>
              <TextField
                id="dummy-input"
                name="menuAmount"
                variant="outlined"
                fullWidth
              />
            </Skeleton>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Skeleton>
              <Typography variant="overline" gutterBottom>
                Dummy header
              </Typography>
            </Skeleton>
            <Skeleton>
              <TextField
                id="dummy-input"
                name="menuAmount"
                variant="outlined"
                fullWidth
              />
            </Skeleton>
            <Skeleton>
              <TextField
                id="dummy-input"
                name="menuAmount"
                variant="outlined"
                fullWidth
              />
            </Skeleton>
          </Paper>
        </Grid>
      </Grid>
    </div>
  )
}

export default DummySkeleton
