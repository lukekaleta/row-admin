import {
  Grid,
  Paper,
  Typography,
  TextField,
  Switch,
  Button,
  FormControlLabel,
  InputAdornment,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import Tag from '../../components/Tag'
import allergens from '../../lib/local-storage/static-data/allergens'
import { IDailyMenu } from '../../types/daily-menu.types'
import { partsStyles } from './DailyMenu@styles'
import { MenuSettingsPropsTypes } from './DailyMenu@types'

const useStyles = makeStyles(partsStyles)

const MenuSettings = (props: MenuSettingsPropsTypes) => {
  // props
  const { handleRemove, handleUpdate, data, index } = props

  // state
  const [menuData, setMenuData] = useState<IDailyMenu>({
    name: data.name,
    price: data.price,
    amount: data.amount,
    allergens: data.allergens,
    isBusiness: data.isBusiness,
  })

  // hooks
  const classes = useStyles()
  const { t } = useTranslation()

  /**
   * If is allergen in state, system detect active allergen
   * @param identifier
   * @returns
   */
  const checkActiveAllergen = (identifier: string): boolean => {
    const arr: string[] = [...menuData.allergens]
    return arr.includes(identifier) || false
  }

  /**
   * If is active allergen and user click on allergen again,
   * system will remove that allergen from state so result is
   * current active allergens. Also if user click on unactive allergen
   * system will add that allergen to state and results is active allergens
   * @param identifier
   * @returns
   */
  const handleAddAllergen = (identifier: string): void => {
    if (checkActiveAllergen(identifier)) {
      return setMenuData((prevState) => ({
        ...prevState,
        allergens: menuData.allergens.filter((item) => {
          return item !== identifier
        }),
      }))
    } else {
      return setMenuData((prevState) => ({
        ...prevState,
        allergens: [...menuData.allergens, identifier],
      }))
    }
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMenuData({
      ...menuData,
      [event.target.name]: event.target.value,
    })
  }

  const handleSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMenuData({
      ...menuData,
      [event.target.name]: event.target.checked,
    })
  }

  return (
    <Grid item xs={12}>
      <Paper className={classes.paper}>
        <Typography variant="overline" gutterBottom>
          {`${t`Daily_Menu.Menu`} ${index + 1}`}
        </Typography>
        <div className={classes.spacing}>
          <Grid container spacing={1}>
            <Grid item md={2} xs={12}>
              <TextField
                id={`menu-amount${index}`}
                name="amount"
                label={t`Daily_Menu.Amount`}
                variant="outlined"
                value={menuData.amount}
                onChange={handleChange}
                fullWidth
                type="number"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">l</InputAdornment>
                  ),
                }}
              />
            </Grid>
            <Grid item md={8} xs={12}>
              <TextField
                id={`menu-name${index}`}
                name="name"
                label={t`Daily_Menu.Menu_Name`}
                variant="outlined"
                value={menuData.name}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item md={2} xs={12}>
              <TextField
                id={`menu-price${index}`}
                name="price"
                label={t`Daily_Menu.Price`}
                variant="outlined"
                value={menuData.price}
                onChange={handleChange}
                type="number"
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">Kč</InputAdornment>
                  ),
                }}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <div className={classes.left}>
                <Typography variant="subtitle1">
                  {t('Daily_Menu.Allergens') + ':'}
                </Typography>
                {allergens.map((a) => (
                  <Tag
                    key={a.number}
                    identifier={a.identifier}
                    label={String(a.number)}
                    tooltip={t(`Common.Allergens.${a.loc}`)}
                    onClick={handleAddAllergen}
                    isActive={checkActiveAllergen(a.identifier)}
                  />
                ))}
              </div>
            </Grid>
            <Grid item md={2} xs={12} className={classes.center}>
              <FormControlLabel
                id={`menu-has-business${index}`}
                control={
                  <Switch
                    color="primary"
                    name="isBusiness"
                    checked={menuData.isBusiness}
                    onChange={handleSwitch}
                  />
                }
                label={t`Daily_Menu.Business`}
              />
            </Grid>
            <Grid item md={2} xs={12} className={classes.center}>
              <Button
                variant="text"
                color="secondary"
                onClick={() => handleRemove(data.id)}
                fullWidth
              >
                {t`Global.Remove`}
              </Button>
            </Grid>
            <Grid item md={2} xs={12} className={classes.center}>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                onClick={() => handleUpdate(data.id, menuData, index + 1)}
              >
                {t`Global.Save`}
              </Button>
            </Grid>
          </Grid>
        </div>
      </Paper>
    </Grid>
  )
}

export default MenuSettings
