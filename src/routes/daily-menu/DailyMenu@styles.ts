import { createStyles, Theme } from '@material-ui/core'

export const dailyMenuStyles = (theme: Theme) =>
  createStyles({
    root: {},
    tabPanel: {
      padding: theme.spacing(3, 0),
    },
  })

export const partsStyles = (theme: Theme) =>
  createStyles({
    root: {},
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
      height: 'auto',
    },
    spacing: {
      marginTop: theme.spacing(2),
    },
    center: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
    },
    skretch: {
      display: 'flex',
      alignItems: 'skretch',
    },
    left: {
      display: 'flex',
      alignItems: 'center',
      height: '100%',
      flexWrap: 'wrap',
    },
  })
