import { ContactsModel } from './../../models/contacts.model';

export type ContactsStateTypes = {
  loading: boolean
  noteDialog: boolean
  noteDialogEditable: boolean
  newNote: string | null
  rowData: ContactsModel | undefined
}