import {
  FormControlLabel,
  Grid,
  makeStyles,
  Switch,
  TextField,
} from '@material-ui/core'
import { Formik, useFormik } from 'formik'
import { useTranslation } from 'react-i18next'
import { Button } from '../../components/Button'
import { Modal } from '../../components/Modal'
import { formatInitials } from '../../utils/form/forms'
import {
  createFormInsertDomainSchema,
  formInsertDomainInitials,
} from './Domains@config'
import { DomainsStyles } from './Domains@styles'
import { InsertModalPropsTypes } from './Domains@types'

const useStyles = makeStyles(DomainsStyles)

export const InsertModal = (props: InsertModalPropsTypes) => {
  const { isOpen, handleCloseModal, handleInsert } = props

  const classes = useStyles()
  const { t } = useTranslation()

  // formik
  const formik: any = useFormik({
    initialValues: formatInitials(formInsertDomainInitials),
    validationSchema: createFormInsertDomainSchema(),
    onSubmit: () => handleInsert(formik.values),
  })

  return (
    <Modal open={isOpen} handleClose={handleCloseModal}>
      <div className={classes.modal}>
        <Formik
          initialValues={formik.initialValues}
          validationSchema={formik.validationSchema}
          onSubmit={() => formik.handleSubmit()}
        >
          {({ handleChange, handleSubmit }) => {
            return (
              <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="standard"
                      margin="normal"
                      id="domain"
                      name="domain"
                      label={t`Domains.Domain`}
                      value={formik.values.domain}
                      onChange={handleChange}
                      autoComplete="domain"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="standard"
                      margin="normal"
                      id="operator"
                      name="operator"
                      label={t`Domains.Operator`}
                      value={formik.values.operator}
                      onChange={handleChange}
                      autoComplete="operator"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="standard"
                      margin="normal"
                      id="phone"
                      name="phone"
                      label={t`Domains.Phone`}
                      value={formik.values.phone}
                      onChange={handleChange}
                      autoComplete="phone"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="standard"
                      margin="normal"
                      id="email"
                      name="email"
                      label={t`Domains.Email`}
                      value={formik.values.email}
                      onChange={handleChange}
                      autoComplete="email"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="standard"
                      margin="normal"
                      id="hosted"
                      name="hosted"
                      label={t`Domains.Hosted`}
                      value={formik.values.hosted}
                      onChange={handleChange}
                      autoComplete="hosted"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      type="number"
                      variant="standard"
                      margin="normal"
                      id="expiration"
                      name="expiration"
                      label={t`Domains.Expiration`}
                      value={formik.values.expiration}
                      onChange={handleChange}
                      autoComplete="expiration"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <FormControlLabel
                      value="top"
                      label={t`Domains.Paid`}
                      labelPlacement="start"
                      control={
                        <Switch
                          onChange={handleChange}
                          name="is_paid"
                          id="is_paid"
                          value={formik.values.is_paid}
                        />
                      }
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                    >
                      {t`Global.Send`}
                    </Button>
                  </Grid>
                </Grid>
              </form>
            )
          }}
        </Formik>
      </div>
    </Modal>
  )
}
