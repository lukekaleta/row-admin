export type InsertModalPropsTypes = {
  isOpen: boolean
  handleCloseModal: () => void
  handleInsert: (values: any) => void
}

export type DomainsStateTypes = {
  loading: boolean
  insertModal: boolean
}
