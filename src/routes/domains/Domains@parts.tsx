import { Grid, makeStyles, TextField } from '@material-ui/core'
import { Formik } from 'formik'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '../../components/Button'
import { Modal } from '../../components/Modal'
import { DomainsModel } from '../../models/domains.model'
import {
  createFormInsertDomainSchema,
  formInsertDomainInitials
} from './Domains@config'
import { DomainsStyles } from './Domains@styles'
import { InsertModalPropsTypes } from './Domains@types'

const useStyles = makeStyles(DomainsStyles)

export const InsertModal = (props: InsertModalPropsTypes) => {
  const [loading, setLoading] = useState<boolean>(false)
  const { isOpen, handleCloseModal, handleInsert } = props

  const classes = useStyles()
  const { t } = useTranslation()

  const handleSubmit = async (values: DomainsModel) => {
    try {
      setLoading(true)
      await handleInsert(values)
      handleCloseModal()
    } catch (err) {
      console.error(err)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal open={isOpen} handleClose={handleCloseModal}>
      <div className={classes.modal}>
        <Formik
          initialValues={formInsertDomainInitials}
          validationSchema={createFormInsertDomainSchema()}
          onSubmit={(values) => handleSubmit(values)}
        >
          {({ values, handleChange, handleBlur, handleSubmit, errors }) => {
            return (
              <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="filled"
                      margin="normal"
                      label={t`Domains.Domain`}
                      type="domain"
                      name="domain"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.domain}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="filled"
                      margin="normal"
                      label={t`Domains.Operator`}
                      type="operator"
                      name="operator"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.operator}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="filled"
                      margin="normal"
                      label={t`Domains.Phone`}
                      type="phone"
                      name="phone"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.phone}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="filled"
                      margin="normal"
                      label={t`Domains.Email`}
                      type="email"
                      name="email"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="filled"
                      margin="normal"
                      label={t`Domains.Hosted`}
                      type="hosted"
                      name="hosted"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.hosted}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="filled"
                      margin="normal"
                      label={t`Domains.Expiration`}
                      name="expiration"
                      type="date"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.expiration}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                    >
                      {t`Global.Send`}
                    </Button>
                  </Grid>
                </Grid>
              </form>
            )
          }}
        </Formik>
      </div>
    </Modal>
  )
}
