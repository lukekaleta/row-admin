import {
  Avatar,
  Container,
  CssBaseline,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/styles'
import { Form, Formik, useFormik } from 'formik'
import { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '../../components/Button'
import { ErrorSummary } from '../../components/ErrorSummary'
import { Loading } from '../../components/Loading'
import { AuthContext } from '../../providers/auth.provider'
import { createFormLoginSchema } from './Login@config'
import { LoginStyles } from './Login@styles'

const useStyles = makeStyles(LoginStyles)

const Login = () => {
  // hooks
  const { t } = useTranslation()
  const classes = useStyles()
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: createFormLoginSchema(),
    onSubmit: () => handleLogin(),
  })

  // context
  const { login, loading } = useContext(AuthContext)

  const handleLogin = () => {
    login(formik.values)
  }

  if (loading) {
    return (
      <div className={classes.root}>
        <Loading description={t`Login.Logged_In`} />
      </div>
    )
  }

  return (
    <div className={classes.root}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5" className={classes.header}>
            {t`Login.Sign_In_To_Administration`}
          </Typography>
          <Formik
            initialValues={formik.initialValues}
            onSubmit={() => formik.handleSubmit()}
          >
            <Form>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    required
                    variant="outlined"
                    margin="normal"
                    id="email"
                    name="email"
                    label={t`Login.Email_Address`}
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    autoComplete="username"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    required
                    id="password"
                    type="password"
                    variant="outlined"
                    margin="normal"
                    autoComplete="current-password"
                    label={t`Login.Password`}
                    value={formik.values.password}
                    onChange={formik.handleChange}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    color="primary"
                    variant="contained"
                    type="submit"
                    fullWidth
                  >
                    {t`Login.Sign_In`}
                  </Button>
                </Grid>
              </Grid>
            </Form>
          </Formik>
        </div>
      </Container>
      {loading && <Loading />}
    </div>
  )
}

export default Login
