import {
  Avatar,
  CssBaseline,
  Grid,
  TextField,
  Paper,
  Typography,
  Link,
} from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/styles'
import { Form, Formik, useFormik } from 'formik'
import { useState } from 'react'
import { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '../../components/Button'
import { Loading } from '../../components/Loading'
import { AuthContext } from '../../providers/auth.provider'
import {
  createFormLoginSchema,
  createFormForgotPasswordSchema,
} from './Login@config'
import { LoginStyles } from './Login@styles'
import { AlertsContext } from '../../providers/alerts.provider'
import { AlertTypes } from '../../types/alerts.types'
import guid from '../../utils/guid'

const useStyles = makeStyles(LoginStyles)

const Login = () => {
  const [forgetPassword, setForgetPassword] = useState(false)

  // hooks
  const { t } = useTranslation()
  const classes = useStyles()
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: createFormLoginSchema(),
    onSubmit: () => handleLogin(),
  })

  const formikForgotPassword = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: createFormForgotPasswordSchema(),
    onSubmit: () => handleSendEmail(),
  })

  // context
  const { login, loading, sendPasswordResetEmail } = useContext(AuthContext)
  const { setAlert } = useContext(AlertsContext)

  const handleLogin = async () => {
    try {
      await login(formik.values)
    } catch (err) {
      // console.error(err)
      await setAlert({
        title: t`Alert.Error`,
        description: t`Alert.Logged_In_Error`,
        type: AlertTypes.success,
        id: guid(),
      })
    }
  }

  const handleSendEmail = async () => {
    try {
      await sendPasswordResetEmail(formikForgotPassword.values)
      await setForgetPassword(false)
      await setAlert({
        title: t`Alert.Success`,
        description: t`Alert.Email_Was_Send`,
        type: AlertTypes.success,
        id: guid(),
      })
    } catch (err) {
      console.error(err)
    }
  }

  if (loading) {
    return (
      <div className={classes.root}>
        <Loading description={t`Login.Logged_In`} />
      </div>
    )
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>

          <Typography component="h1" variant="h5">
            {forgetPassword
              ? t`Login.Forgot_Password_Header`
              : t`Login.Sign_In_To_Administration`}
          </Typography>

          {forgetPassword ? (
            <Formik
              initialValues={formikForgotPassword.initialValues}
              onSubmit={() => formikForgotPassword.handleSubmit()}
            >
              <Form style={{ width: '100%' }}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="outlined"
                      margin="normal"
                      id="email"
                      name="email"
                      label={t`Login.Email_Address`}
                      value={formikForgotPassword.values.email}
                      onChange={formikForgotPassword.handleChange}
                      autoComplete="username"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Alert severity="info">{t`Login.Forgot_Password_Description`}</Alert>
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      color="primary"
                      variant="contained"
                      type="submit"
                      fullWidth
                    >
                      {t`Global.Send`}
                    </Button>
                  </Grid>
                </Grid>

                <Grid container spacing={2}>
                  <Grid item xs />
                  <Grid item>
                    <Link
                      className={classes.link}
                      onClick={() => setForgetPassword(!forgetPassword)}
                      color="secondary"
                      variant="body2"
                    >
                      {t`Login.Back_To_Login`}
                    </Link>
                  </Grid>
                </Grid>
              </Form>
            </Formik>
          ) : (
            <Formik
              initialValues={formik.initialValues}
              onSubmit={() => formik.handleSubmit()}
            >
              <Form style={{ width: '100%' }}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      variant="outlined"
                      margin="normal"
                      id="email"
                      name="email"
                      label={t`Login.Email_Address`}
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      autoComplete="username"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      required
                      id="password"
                      type="password"
                      variant="outlined"
                      margin="normal"
                      autoComplete="current-password"
                      label={t`Login.Password`}
                      value={formik.values.password}
                      onChange={formik.handleChange}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      color="primary"
                      variant="contained"
                      type="submit"
                      fullWidth
                    >
                      {t`Login.Sign_In`}
                    </Button>
                  </Grid>
                </Grid>

                <Grid container spacing={2}>
                  <Grid item xs />
                  <Grid item>
                    <Link
                      className={classes.link}
                      onClick={() => setForgetPassword(!forgetPassword)}
                      color="secondary"
                      variant="body2"
                    >
                      {t`Login.Forgot_Password`}
                    </Link>
                  </Grid>
                </Grid>
              </Form>
            </Formik>
          )}
        </div>
      </Grid>
    </Grid>
  )
}

export default Login
