import { QuestionsModel } from './../../models/questions.model';
export type QuestionsStateTypes = {
  loading: boolean
  noteDialog: boolean
  rowData: QuestionsModel | undefined
}