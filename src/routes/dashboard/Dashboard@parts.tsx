import { makeStyles } from '@material-ui/styles'
import { Card, Grid, Typography } from '@material-ui/core'
import { dashboardCardStyles } from './Dashboard@styles'
import { DashboardCardPropsTypes } from './Dashboard@types'
import TrendingUpIcon from '@material-ui/icons/TrendingUp'

const useStyles = makeStyles(dashboardCardStyles)

export const DashboardCard = (props: DashboardCardPropsTypes) => {
  // props
  const { title, number, color, icon } = props

  // hooks
  const classes = useStyles()

  return (
    <Card className={classes.card}>
      <Grid container>
        <Grid item xs={12}>
          <Grid container>
            <Grid item xs={2}>
              {icon}
            </Grid>
            <Grid item xs={10} className={classes.right}>
              <Typography
                className={classes.title}
                variant="subtitle2"
                display="block"
                gutterBottom
              >
                {title}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h4" className={classes.number}>
            {number}
          </Typography>
        </Grid>
      </Grid>
    </Card>
  )
}
