import { IconButton, makeStyles, Paper } from '@material-ui/core'
import { ModalStyles } from './Modal@styles'
import { Modal as MuiModal } from '@material-ui/core'
import { ModalPropsTypes } from './Modal@types'
import CloseIcon from '@material-ui/icons/Close'

const useStyles = makeStyles(ModalStyles)

const Modal = (props: ModalPropsTypes) => {
  const { open, handleClose, children } = props

  const classes = useStyles()

  return (
    <MuiModal open={open} className={classes.root}>
      <Paper className={classes.paper}>
        <div className={classes.closeIcon}>
          <IconButton size="small" onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </div>
        {children}
      </Paper>
    </MuiModal>
  )
}

export default Modal
