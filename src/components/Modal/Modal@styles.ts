import { createStyles, Theme } from '@material-ui/core'

export const ModalStyles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      boxShadow: theme.shadows[5],
      padding: theme.spacing(6, 4, 6),
      minWidth: 450,
      position: 'relative'
    },
    closeIcon: {
      position: 'absolute',
      top: 10,
      right: 10
    }
  })
