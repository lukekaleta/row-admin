import { createStyles, Theme } from '@material-ui/core'

export const ErrorSummaryStyles = (theme: Theme) =>
  createStyles({
    root: () => ({
      ...theme.typography.h6,
      // color: props.color,
      // background: props.background,
      borderRadius: theme.shape.borderRadius,
      textAlign: 'left',
    }),
    heading: {
      ...theme.typography.h5,
      paddingBottom: theme.spacing(1),
    },
    list: {
      paddingLeft: theme.spacing(2),
      '& > li': {
        listStyle: 'disc',
      },
    },
  })
