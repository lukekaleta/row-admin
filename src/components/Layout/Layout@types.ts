import { ReactNode } from 'react'
import { Themes } from '../../styles/Styles@types'

export type LayoutProps = {
  children: ReactNode
}

export type LayoutStylesPropsTypes = {
  getSiderNavWidth: number
}

export type DrawerContentPropsTypes = {
  getSiderNavWidth: number
  logoUrl: string
  getFirstLetterOfUsername: string
  handleDrawerAction: () => void
  userData: any
}
