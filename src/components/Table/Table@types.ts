import { MaterialTableProps } from 'material-table';

export interface TablePropsTypes extends MaterialTableProps<object> {
}

export type TableCheckIconPropsTypes = {
  rowItem: boolean
}

export type TableToolbarPropTypes = {
  header: string
}

export type TableToolbarStateTypes = {
  settingsMenu: boolean
  filtering: boolean
  grouping: boolean
}